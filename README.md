<div align="center">
    <h1>Hi there, I'm Emmanuil <img src="https://github.com/blackcater/blackcater/raw/main/images/Hi.gif" height="32"/></h1>
    <h3 align="center">Unity C# Developer</h3>


[![LinkedIn](https://img.shields.io/badge/LinkedIn-blue?style=for-the-badge&logo=LinkedIn)](https://www.linkedin.com/in/emmanuil-andreev-6809a079)
[![Telegram](https://img.shields.io/badge/telegram-blue?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/EmmanuilAndreev)
<a href="mailto:andreev.emmanuil@gmail.com"><img src="https://img.shields.io/badge/andreev.emmanuil%40gmail.com-blue?style=for-the-badge&logo=gmail&logoColor=white"/></a>


<h2>Technology Stack</h2>

### Languages

![C-Sharp](https://img.shields.io/badge/C--Sharp-gray?style=for-the-badge&logo=CSharp&logoColor=%23f719da)
![Python](https://img.shields.io/badge/Python-gray?style=for-the-badge&logo=python&logoColor=yellow)

### Programs

![Unity](https://img.shields.io/badge/Unity-gray?style=for-the-badge&logo=Unity&logoColor=lightgray)
![Rider](https://img.shields.io/badge/rider-gray?style=for-the-badge&logo=Rider&logoColor=orange)
![Visual Studio](https://img.shields.io/badge/Visual%20studio-gray?style=for-the-badge&logo=visual%20studio&logoColor=%23f719da)
![Git](https://img.shields.io/badge/Git-gray?style=for-the-badge&logo=git)
![Blender](https://img.shields.io/badge/blender-gray?style=for-the-badge&logo=Blender)
![Photoshop](https://img.shields.io/badge/Photoshop-gray?style=for-the-badge&logo=adobe%20photoshop)


## Games I Worked On

#### **Snowball Defense** 
[<img align="center" src="https://img.shields.io/badge/GitLab-gray?style=for-the-badge&logo=gitlab"/>](https://gitlab.com/muuulya/snowball-defense) [<img align="center" src="https://img.shields.io/badge/Itch-gray?style=for-the-badge&logo=itchdotio"/>](https://immod.itch.io/snowball-defense)

#### **Cannon & Cannons**
[<img align="center" src="https://img.shields.io/badge/gitlab-gray?style=for-the-badge&logo=gitlab"/>](https://gitlab.com/kabarga-games/c-and-c) [<img align="center" src="https://img.shields.io/badge/Itch-gray?style=for-the-badge&logo=itchdotio"/>](https://immod.itch.io/cannoncannons)

#### **Door-ka**
[<img align="center" src="https://img.shields.io/badge/Gitlab-gray?style=for-the-badge&logo=gitlab"/>](https://gitlab.com/kabarga-games/onelevel) [<img align="center" src="https://img.shields.io/badge/itch-gray?style=for-the-badge&logo=itchdotio"/>](https://immod.itch.io/door-ka)

#### **Ха, Я украл твои данные!**
[<img align="center" src="https://img.shields.io/badge/Gitlab-gray?style=for-the-badge&logo=gitlab"/>](https://gitlab.com/kabarga-games/mamkerhacker) [<img align="center" src="https://img.shields.io/badge/itch-gray?style=for-the-badge&logo=itchdotio"/>](https://immod.itch.io/haistoleyourdata)

#### **ONE MORE DAY: THE РУСЫ** 
[<img align="center" src="https://img.shields.io/badge/Gitlab-gray?style=for-the-badge&logo=gitlab"/>](https://gitlab.com/kabarga-games/rusi-protiv-yacherov) [<img align="center" src="https://img.shields.io/badge/itch-gray?style=for-the-badge&logo=itchdotio"/>](https://muuulya.itch.io/one-more-day-the-pycbi)

#### **Числовой кликер 2048** 
[<img align="center" src="https://img.shields.io/badge/Gitlab-gray?style=for-the-badge&logo=gitlab"/>](https://gitlab.com/muuulya/2048) [<img align="center" src="https://img.shields.io/badge/yandex_Games-gray?style=for-the-badge&logo=docsify&logoColor=red"/>](https://yandex.ru/games/app/266750)

#### **Кубы 2048**
[<img align="center" src="https://img.shields.io/badge/Gitlab-gray?style=for-the-badge&logo=gitlab"/>](https://gitlab.com/muuulya/2048-cube-3d)
[<img align="center" src="https://img.shields.io/badge/yandex_Games-gray?style=for-the-badge&logo=docsify&logoColor=red"/>](https://yandex.ru/games/app/286156?utm_source%253Dgame_popup_menu)



</div>